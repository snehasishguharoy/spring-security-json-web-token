/**
 * 
 */
package com.jwt.security.demojwtsecurity.dto;

/**
 * @author sony
 *
 */
public class JwtUser {

	private String userName;
	private Long userId;
	private String role;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
