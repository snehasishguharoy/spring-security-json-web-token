/**
 * 
 */
package com.jwt.security.demojwtsecurity.security;

import org.springframework.stereotype.Component;

import com.jwt.security.demojwtsecurity.dto.JwtUser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author sony
 *
 */
@Component
public class JwtTokenGenerator {

	public String generateToken(JwtUser jwtUser) {
		Claims claims = Jwts.claims().setSubject(jwtUser.getUserName());
		claims.put("userId", String.valueOf(jwtUser.getUserId()));
		claims.put("role", jwtUser.getRole());
		return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, "Delta").compact();
	}

}
