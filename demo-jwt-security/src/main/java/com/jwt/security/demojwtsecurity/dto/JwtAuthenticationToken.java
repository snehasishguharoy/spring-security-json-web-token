/**
 * 
 */
package com.jwt.security.demojwtsecurity.dto;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * The Class JwtAuthenticationToken.
 *
 * @author sony
 */
public class JwtAuthenticationToken extends UsernamePasswordAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The token. */
	private String token;

	/**
	 * Instantiates a new jwt authentication token.
	 *
	 * @param token the token
	 */
	public JwtAuthenticationToken(String token) {
		super(null, null);
		this.setToken(token);
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return null;
	}
}
