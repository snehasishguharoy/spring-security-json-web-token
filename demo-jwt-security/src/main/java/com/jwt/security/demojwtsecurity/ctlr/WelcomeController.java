/**
 * 
 */
package com.jwt.security.demojwtsecurity.ctlr;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sony
 *
 */
@RestController
public class WelcomeController {
	
	@RequestMapping(value="/hello",method=RequestMethod.GET)
	public String sayHello(){
		return "Hello World";
	}

}
