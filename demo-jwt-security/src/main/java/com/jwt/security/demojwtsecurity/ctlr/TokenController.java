/**
 * 
 */
package com.jwt.security.demojwtsecurity.ctlr;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.security.demojwtsecurity.dto.JwtUser;
import com.jwt.security.demojwtsecurity.security.JwtTokenGenerator;

/**
 * @author sony
 *
 */
@RestController
public class TokenController {

	private JwtTokenGenerator generator;

	public TokenController(JwtTokenGenerator generator) {
		this.generator = generator;
	}

	@RequestMapping(value = "/token", method = RequestMethod.POST)
	public String generateToken(@RequestBody JwtUser jwtUser) {

		return generator.generateToken(jwtUser);

	}

}
