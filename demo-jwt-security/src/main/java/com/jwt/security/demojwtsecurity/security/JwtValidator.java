/**
 * 
 */
package com.jwt.security.demojwtsecurity.security;

import org.springframework.stereotype.Component;

import com.jwt.security.demojwtsecurity.dto.JwtUser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

// TODO: Auto-generated Javadoc
/**
 * The Class JwtValidator.
 *
 * @author sony
 */
@Component
public class JwtValidator {

	/** The secrect. */
	private String secrect = "Delta";

	/**
	 * Validate.
	 *
	 * @param token the token
	 * @return the object
	 */
	public Object validate(String token) {
		JwtUser jwtUser = null;
		try {
			Claims claims = Jwts.parser().setSigningKey(secrect).parseClaimsJws(token).getBody();

			jwtUser = new JwtUser();
			jwtUser.setUserName(claims.getSubject());
			jwtUser.setUserId(Long.parseLong((String) claims.get("userId")));
			jwtUser.setRole((String) claims.get("role"));

		} catch (Exception e) {
			System.out.println(e);
		}
		return jwtUser;
	}
}
